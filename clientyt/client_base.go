package clientyt

import (
	"context"
	"errors"
	"fmt"
	"gitee.com/zhancaihua/goyt/core/discover"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/resolver"

	"strings"
	"time"
)

func init() {
	resolver.Register(discover.NewEtcdSubscriber())
	resolver.Register(&discover.DirectBuilder{})
}

const (
	dialTimeout = time.Second * 3
	separator   = '/'
)

type (

	// Client interface wraps the Conn method.
	Client interface {
		Conn() *grpc.ClientConn
	}

	// A ClientOptions is a client options.
	ClientOptions struct {
		NonBlock    bool
		DialOptions []grpc.DialOption
	}

	// ClientOption defines the method to customize a ClientOptions.
	ClientOption func(options *ClientOptions)

	client struct {
		conn *grpc.ClientConn
		op   ClientOptions
	}
)

// WithDialOption returns a func to customize a ClientOptions with given dial option.
func WithDialOption(opt grpc.DialOption) ClientOption {
	return func(options *ClientOptions) {
		options.DialOptions = append(options.DialOptions, opt)
	}
}

// WithNonBlock sets the dialing to be nonblock.
func WithNonBlock() ClientOption {
	return func(options *ClientOptions) {
		options.NonBlock = true
	}
}

// WithStreamClientInterceptor returns a func to customize a ClientOptions with given interceptor.
func WithStreamClientInterceptor(interceptor grpc.StreamClientInterceptor) ClientOption {
	return func(options *ClientOptions) {
		options.DialOptions = append(options.DialOptions,
			grpc.WithChainStreamInterceptor(interceptor))
	}
}

// WithUnaryClientInterceptor returns a func to customize a ClientOptions with given interceptor.
func WithUnaryClientInterceptor(interceptor grpc.UnaryClientInterceptor) ClientOption {
	return func(options *ClientOptions) {
		options.DialOptions = append(options.DialOptions,
			grpc.WithChainUnaryInterceptor(interceptor))
	}
}

// NewClient returns a Client.
func NewClient(target string, opts ...ClientOption) (Client, error) {
	var options []ClientOption
	options = append(options, opts...)
	//默认round_robin
	svcCfg := `{"loadBalancingConfig":[{"round_robin":{}}]}`
	balancerOpt := WithDialOption(grpc.WithDefaultServiceConfig(svcCfg))
	options = append(options, balancerOpt)

	cli := &client{}
	for _, o := range options {
		o(&cli.op)
	}

	if err := cli.dial(target); err != nil {
		return nil, err
	}

	return cli, nil
}

func (c *client) Conn() *grpc.ClientConn {
	return c.conn
}
func (c *client) dial(server string) error {
	options := c.buildDialOptions()
	timeCtx, cancel := context.WithTimeout(context.Background(), dialTimeout)
	defer cancel()
	conn, err := grpc.DialContext(timeCtx, server, options...)
	if err != nil {
		service := server
		if errors.Is(err, context.DeadlineExceeded) {
			pos := strings.LastIndexByte(server, separator)
			// len(server) - 1 is the index of last char
			if 0 < pos && pos < len(server)-1 {
				service = server[pos+1:]
			}
		}
		return fmt.Errorf("rpc dial: %s, error: %s, make sure rpc service %q is already started",
			server, err.Error(), service)
	}

	c.conn = conn
	return nil
}

func (c *client) buildDialOptions(opts ...ClientOption) []grpc.DialOption {
	cliOpts := c.op
	var options []grpc.DialOption
	//目前先默认不支持tls rpc
	options = append([]grpc.DialOption(nil),
		grpc.WithTransportCredentials(insecure.NewCredentials()))

	if !cliOpts.NonBlock {
		options = append(options, grpc.WithBlock())
	}

	return append(options, cliOpts.DialOptions...)
}
