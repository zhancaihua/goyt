package httpyt

import (
	"gitee.com/zhancaihua/goyt/core/logyt"
	"gitee.com/zhancaihua/goyt/httpyt/middleware"
	"github.com/gin-gonic/gin"
	"net/http"
	"testing"
)

func TestHttps(t *testing.T) {
	logyt.SetUpSugar(logyt.SugarConf{
		ServiceName: "rpc",
		Debug:       true,
	})
	s, err := NewHttpServerYt(WithMode(Release), WithAddr("localhost:443"), WithTLS(), WithCertPem("tls-rootca.pem"), WithKeyPem("tls-rootca.key.pem"))
	if nil != err {
		t.Fatal(err)
	}
	s.Use(middleware.Logger(), middleware.Recovery())

	s.GET("test/tls", func(context *gin.Context) {
		context.String(http.StatusOK, "this is tls from %s", context.ClientIP())
		panic("panic here")
	})

	err = s.Start()
	if nil != err {
		t.Fatal(err)
	}
}
func TestHttpServerConfig(t *testing.T) {
	s, err := SetUpServer(HttpServerConf{
		Name:                  "http",
		ListenOn:              ":8080",
		Tls:                   false,
		CertPem:               "",
		KeyPem:                "",
		Mode:                  Debug,
		ReadTimeout:           0,
		ReadHeaderTimeout:     0,
		WriteTimeout:          0,
		IdleTimeout:           0,
		RedirectTrailingSlash: false,
		ForwardedByClientIP:   false,
		TrustedPlatform:       "",
		RemoteIPHeaders:       nil,
		TrustedProxies:        nil,
		Middlewares: HttpServerMiddlewaresConf{
			Recover:           true,
			Logger:            true,
			LoggerIgnorePaths: nil,
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	s.GET("test/tls", func(context *gin.Context) {
		context.String(http.StatusOK, "this is tls from %s", context.ClientIP())
		panic("panic here")
	})
	err = Start(s)
	if err != nil {
		t.Fatal(err)
	}
}
