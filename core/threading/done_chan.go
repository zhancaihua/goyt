package threading

import (
	"gitee.com/zhancaihua/goyt/core/lang"
	"sync"
)

// DoneChan 主要是用于通知任务完成的chan，类似ctx.Done和Cancel
// 使用者调用Close，则Done返回的chan的状态变为已准备
type DoneChan struct {
	done chan lang.PlaceholderType
	once sync.Once
}

// NewDoneChan returns a DoneChan.
func NewDoneChan() *DoneChan {
	return &DoneChan{
		done: make(chan lang.PlaceholderType),
	}
}

// Close closes dc, it's safe to close more than once.
func (dc *DoneChan) Close() {
	dc.once.Do(func() {
		close(dc.done)
	})
}

// Done returns a channel that can be notified on dc closed.
func (dc *DoneChan) Done() chan lang.PlaceholderType {
	return dc.done
}
