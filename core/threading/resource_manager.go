package threading

import (
	"gitee.com/zhancaihua/goyt/core/erroryt"
	"io"
	"sync"
)

// A ResourceManager 主要是用来管理那种加载初始化一次就可以供所有线程使用的并且需要释放东西。比如etcd的连接,mysql的客户端等
// 利用resourceManager可以优雅的管理我们的资源，而不用很多资源都设置为全局变量
type ResourceManager struct {
	resources    map[string]io.Closer //获得资源
	singleFlight SingleFlight         //用来控制初始化一次
	lock         sync.RWMutex
}

// NewResourceManager returns a ResourceManager.
func NewResourceManager() *ResourceManager {
	return &ResourceManager{
		resources:    make(map[string]io.Closer),
		singleFlight: NewSingleFlight(),
	}
}

// Close closes the manager.
// Don't use the ResourceManager after Close() called.
func (manager *ResourceManager) Close() error {
	manager.lock.Lock()
	defer manager.lock.Unlock()

	var be erroryt.BatchError
	for _, resource := range manager.resources {
		if err := resource.Close(); err != nil {
			be.Add(err)
		}
	}

	// release resources to avoid using it later
	manager.resources = nil

	return be.Err()
}

// GetResource returns the resource associated with given key.
func (manager *ResourceManager) GetResource(key string, create func() (io.Closer, error)) (io.Closer, error) {
	val, err := manager.singleFlight.Do(key, func() (any, error) { //只初始化一次
		manager.lock.RLock() //上读锁。如果资源存在直接返回
		resource, ok := manager.resources[key]
		manager.lock.RUnlock()
		if ok {
			return resource, nil
		}

		resource, err := create()
		if err != nil {
			return nil, err
		}

		manager.lock.Lock() //写锁新建资源
		defer manager.lock.Unlock()
		manager.resources[key] = resource

		return resource, nil //返回
	})
	if err != nil {
		return nil, err
	}

	return val.(io.Closer), nil
}

// Inject 插入资源。
func (manager *ResourceManager) Inject(key string, resource io.Closer) {
	manager.lock.Lock() //写锁
	manager.resources[key] = resource
	manager.lock.Unlock()
}
