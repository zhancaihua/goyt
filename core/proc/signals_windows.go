//go:build windows

package proc

import (
	"gitee.com/zhancaihua/goyt/core/logyt"
	"os"
	"os/signal"
	"syscall"
)

// done 保证gracefulStop只调用一次，避免后来的多次信号导致调用多次
var done = make(chan struct{})

func init() {
	go func() {

		// https://golang.org/pkg/os/signal/#Notify
		signals := make(chan os.Signal, 1)
		signal.Notify(signals, syscall.SIGINT)

		for {
			v := <-signals
			switch v {
			// windows下抓取ctrl+c ctrl+parse 的interrupt信号即可
			case syscall.SIGINT:
				select {
				case <-done:
					// already closed
				default:
					close(done)
				}

				gracefulStop(signals)
			default:
				logyt.WarnLogF("Got unregistered signal:", v)
			}
		}
	}()
}
