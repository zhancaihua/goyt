package discover

import "sync"

var (
	accounts = make(map[string]Account)
	lock     sync.RWMutex
)

// Account 先在全局管理下etcd集群的用户名密码。通过getClusterKey(endpoints) 唯一映射的id
type Account struct {
	User string
	Pass string
}

// AddAccount adds the username/password for the given etcd cluster.
func AddAccount(endpoints []string, user, pass string) {
	lock.Lock()
	defer lock.Unlock()

	accounts[getClusterKey(endpoints)] = Account{
		User: user,
		Pass: pass,
	}
}

// GetAccount gets the username/password for the given etcd cluster.
func GetAccount(endpoints []string) (Account, bool) {
	lock.RLock()
	defer lock.RUnlock()

	account, ok := accounts[getClusterKey(endpoints)]
	return account, ok
}
