package discover

import (
	"errors"
	clientv3 "go.etcd.io/etcd/client/v3"
	"time"
)

const (
	// Delimiter is a separator that separates the etcd path.
	Delimiter = '/'

	autoSyncInterval   = time.Minute
	dialTimeout        = 5 * time.Second
	dialKeepAliveTime  = 5 * time.Second
	requestTimeout     = 3 * time.Second
	endpointsSeparator = ","
)

var (
	// errEmptyEtcdHosts indicates that etcd hosts are empty.
	errEmptyEtcdHosts = errors.New("empty etcd hosts")
	// errEmptyEtcdKey indicates that etcd key is empty.
	errEmptyEtcdKey = errors.New("empty etcd key")
)

const timeToLive int64 = 10

// TimeToLive etcd租约时间
var TimeToLive = timeToLive

var (
	// DialTimeout is the dial timeout.
	DialTimeout = dialTimeout
	// RequestTimeout is the request timeout.
	RequestTimeout = requestTimeout
	// NewClient is used to create etcd clients.
	NewClient = DialClient
)

func DialClient(endpoints []string) (EtcdClient, error) {
	cfg := clientv3.Config{
		Endpoints:            endpoints,
		AutoSyncInterval:     autoSyncInterval,
		DialTimeout:          DialTimeout,
		DialKeepAliveTime:    dialKeepAliveTime,
		DialKeepAliveTimeout: DialTimeout,
		RejectOldCluster:     true,
		PermitWithoutStream:  true,
	}
	if account, ok := GetAccount(endpoints); ok {
		cfg.Username = account.User
		cfg.Password = account.Pass
	}
	return clientv3.New(cfg)
}
