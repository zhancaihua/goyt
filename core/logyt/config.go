package logyt

type LogConf struct {
	ServiceName      string   `mapstructure:"service_name" json:"service_name,omitempty"`
	LevelStyle       string   `mapstructure:"level_style" json:"level_style,omitempty"`             //lowercase,capital,lowercase_color,capital_color
	CallerStyle      string   `mapstructure:"caller_style" json:"caller_style,omitempty"`           // short,full
	DurationStyle    string   `mapstructure:"duration_style" json:"duration_style,omitempty"`       // second,nano,milli,string
	TimeStyle        string   `mapstructure:"time_style" json:"time_style,omitempty"`               //iso8601,epoch,epoch_millis,epoch_nanos,rfc3339,rfc3339_nano,layout
	LoggerNameStyle  string   `mapstructure:"logger_name_style" json:"logger_name_style,omitempty"` //full_name
	OutStyle         string   `mapstructure:"out_style" json:"out_style,omitempty"`                 //plain,json
	TimeKey          string   `mapstructure:"time_key" json:"time_key,omitempty"`
	LevelKey         string   `mapstructure:"level_key" json:"level_key,omitempty"`
	NameKey          string   `mapstructure:"name_key" json:"name_key,omitempty"`
	CallerKey        string   `mapstructure:"caller_key" json:"caller_key,omitempty"`
	StacktraceKey    string   `mapstructure:"stacktrace_key" json:"stacktrace_key,omitempty"`
	FunctionKey      string   `mapstructure:"function_key" json:"function_key,omitempty"`
	MessageKey       string   `mapstructure:"message_key" json:"message_key,omitempty"`
	LineEnding       string   `mapstructure:"line_ending" json:"line_ending,omitempty"`
	TimeFormat       string   `mapstructure:"time_format" json:"time_format,omitempty"`
	StackLevel       string   `mapstructure:"stack_level" json:"stack_level,omitempty"` // debug info warn error dpanic panic fatal none
	OutputPaths      []string `mapstructure:"output_paths" json:"output_paths,omitempty"`
	LogLevel         string   `mapstructure:"log_level" json:"log_level,omitempty"` // debug info warn error dpanic panic fatal none
	AddCaller        bool     `mapstructure:"add_caller" json:"add_caller,omitempty"`
	OnFatal          string   `mapstructure:"on_fatal" json:"on_fatal,omitempty"` //goexit panic exit
	CallerSkip       int      `mapstructure:"caller_skip" json:"caller_skip,omitempty"`
	ErrorOutputPaths []string `mapstructure:"error_output_paths" json:"error_output_paths,omitempty"`
	Clock            string   `mapstructure:"clock" json:"clock,omitempty"`     //system
	IsTerm           bool     `mapstructure:"is_term" json:"is_term,omitempty"` //输出目的地是终端吗
}
