package logyt

import "testing"

func TestLogSetUp(t *testing.T) {
	err := SetUp(LogConf{
		ServiceName: "test_yt",
		OutStyle:    "json",
		LevelStyle:  "lowercase",
		LevelKey:    "level_yt",
		TimeKey:     "timestamp",
		TimeFormat:  "15:04:05",
		NameKey:     "logger_yt",
		CallerStyle: "full",
		MessageKey:  "msg",
		AddCaller:   false,

		StacktraceKey: "stacktrace",
		StackLevel:    "error",
		CallerSkip:    2,
	})
	if nil != err {
		t.Fatal(err)
	}
	InfoLog("myinfo", LogField{
		Key:   "f",
		Value: "v",
	})
	ErrorLog("myerror", LogField{
		Key:   "f",
		Value: "v",
	})
}
func TestLogSugar(t *testing.T) {
	err := SetUpSugar(SugarConf{
		ServiceName: "sugar_product",
		Debug:       false,
	})
	if nil != err {
		t.Fatal(err)
	}
	InfoLog("myinfo", LogField{
		Key:   "f",
		Value: "v",
	})
	ErrorLog("myerror", LogField{
		Key:   "f",
		Value: "v",
	})
}
func TestLogSugarDev(t *testing.T) {
	err := SetUpSugar(SugarConf{
		ServiceName: "sugar_dev",
		Debug:       true,
	})
	if nil != err {
		t.Fatal(err)
	}
	InfoLog("myinfo", LogField{
		Key:   "f",
		Value: "v",
	})
	ErrorLog("myerror", LogField{
		Key:   "f",
		Value: "v",
	})
}
func TestLogNoneStack(t *testing.T) {
	err := SetUp(LogConf{
		ServiceName:   "test_yt",
		OutStyle:      "json",
		LevelStyle:    "lowercase",
		LevelKey:      "level_yt",
		TimeKey:       "timestamp",
		TimeFormat:    "15:04:05",
		NameKey:       "logger_yt",
		CallerStyle:   "full",
		MessageKey:    "msg",
		AddCaller:     false,
		OnFatal:       "panic",
		StacktraceKey: "stacktrace",
		StackLevel:    "none",
		CallerSkip:    2,
	})
	if nil != err {
		t.Fatal(err)
	}
	InfoLog("myinfo", LogField{
		Key:   "f",
		Value: "v",
	})
	ErrorLog("myerror", LogField{
		Key:   "f",
		Value: "v",
	})
	WarnLog("mywarn", LogField{
		Key:   "f",
		Value: "v",
	})
	DebugLog("mydebug", LogField{
		Key:   "f",
		Value: "v",
	})
	DPanicLog("mydpanic", LogField{
		Key:   "f",
		Value: "v",
	})
	defer func() {
		e := recover()
		t.Logf("recover from fatal level %v", e)
	}()
	defer func() {
		e := recover()
		t.Logf("recover from panic level %v", e)
		FatalLog("myfatal", LogField{
			Key:   "f",
			Value: "v",
		})
	}()

	PanicLog("mypanic", LogField{
		Key:   "f",
		Value: "v",
	})

}
