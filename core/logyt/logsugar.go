package logyt

type SugarConf struct {
	ServiceName      string   `mapstructure:"service_name" json:"service_name,omitempty"`
	OutputPaths      []string `mapstructure:"output_paths" json:"output_paths,omitempty"`
	ErrorOutputPaths []string `mapstructure:"error_output_paths" json:"error_output_paths,omitempty"`
	Debug            bool     `mapstructure:"debug" json:"debug,omitempty"`
}

func ReleaseConf(c SugarConf) LogConf {
	return LogConf{
		IsTerm:           false,
		ServiceName:      c.ServiceName,
		LevelStyle:       "lowercase",
		CallerStyle:      "short",
		LoggerNameStyle:  "full_name",
		OutStyle:         "json",
		TimeKey:          "ts",
		LevelKey:         "level",
		NameKey:          "logger",
		CallerKey:        "caller",
		StacktraceKey:    "stacktrace",
		FunctionKey:      "",
		MessageKey:       "msg",
		LineEnding:       "\n",
		TimeStyle:        "epoch",
		StackLevel:       "error",
		OutputPaths:      c.OutputPaths,
		LogLevel:         "info",
		AddCaller:        true,
		OnFatal:          "exit",
		CallerSkip:       2,
		ErrorOutputPaths: c.ErrorOutputPaths,
		Clock:            "system",
		DurationStyle:    "second",
	}
}
func DebugConf(c SugarConf) LogConf {
	return LogConf{
		IsTerm:          true,
		ServiceName:     c.ServiceName,
		LevelStyle:      "capital_color",
		CallerStyle:     "short",
		LoggerNameStyle: "full_name",
		OutStyle:        "plain",
		TimeKey:         "T",
		LevelKey:        "L",
		NameKey:         "N",
		CallerKey:       "C",
		StacktraceKey:   "S",
		FunctionKey:     "",
		MessageKey:      "M",
		LineEnding:      "\n",
		TimeStyle:       "iso8601",
		StackLevel:      "warn",
		LogLevel:        "debug",
		AddCaller:       true,
		OnFatal:         "exit",
		CallerSkip:      2,
		Clock:           "system",
		DurationStyle:   "string",
	}
}
func SetUpSugar(s SugarConf) error {
	if s.Debug {
		return SetUp(DebugConf(s))
	} else {
		return SetUp(ReleaseConf(s))
	}
}
