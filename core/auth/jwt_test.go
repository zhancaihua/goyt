package auth

import (
	"encoding/json"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"testing"
	"time"
)

type CustomClaims struct {
	ID       int64  `json:"id"`
	NickName string `json:"nick_name"`
	RoleId   int64  `json:"role_id"`
	jwt.RegisteredClaims
}

func TestClaim(t *testing.T) {
	s := time.Unix(1675522040, 0)
	j := NewJWTString("01234567890123456789012345678901", 30)
	var c = &CustomClaims{
		ID:               9527,
		NickName:         "tom",
		RoleId:           1,
		RegisteredClaims: j.NewBasicClaim(s),
	}
	con, _ := json.Marshal(c)
	fmt.Println(string(con))

	strToken, _ := j.CreateTokenHash(jwt.SigningMethodHS256, c)
	if strToken != "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OTUyNywibmlja19uYW1lIjoidG9tIiwicm9sZV9pZCI6MSwiZXhwIjoxNjc1NTIyMDcwLCJuYmYiOjE2NzU1MjIwNDAsImlhdCI6MTY3NTUyMjA0MH0.VqP3blpp3K7Ae0qCxBqUN9bP26ynYWZ84rr57h2O118" {
		t.Error("strToken not equal")
	}
}
func TestParse(t *testing.T) {
	s := time.Now()
	j := NewJWTString("01234567890123456789012345678901", 30)
	var c = &CustomClaims{
		ID:               9527,
		NickName:         "tom",
		RoleId:           1,
		RegisteredClaims: j.NewBasicClaim(s),
	}
	con, _ := json.Marshal(c)
	fmt.Println(string(con))
	strToken, _ := j.CreateTokenHash(jwt.SigningMethodHS256, c)
	fmt.Println(strToken)
	cc := &CustomClaims{}
	token, err := j.ParseToken(cc, strToken)
	if err != nil {
		t.Error(fmt.Sprintf("err nil expected but %s", err.Error()))
	}
	rea, _ := token.(*CustomClaims)
	if rea.ID != c.ID || rea.NickName != c.NickName || rea.RoleId != c.RoleId || !rea.ExpiresAt.Time.Equal(c.ExpiresAt.Time) {
		t.Error("token is not equal from source")
	}
}
