package serveryt

import (
	"gitee.com/zhancaihua/goyt/core/discover"
	"testing"
	"time"
)

func TestServer_Start(t *testing.T) {
	server := newRpcServer(WithAddr(":8080"), WithHealth(), WithMiddleware(ServerMiddlewaresConf{Recover: true}), WithName("rpc"))
	time.AfterFunc(time.Second*6, func() {
		_ = server.Stop()
	})
	if err := server.Start(nil); nil != err {
		t.Fatal(err)
	}
}
func TestServer_PubStart(t *testing.T) {
	server := newRpcServer(WithAddr(":8080"), WithHealth(), WithMiddleware(ServerMiddlewaresConf{Recover: true}), WithName("rpc"))
	if err := server.Visit(NewServerPubVisitor(discover.EtcdConf{
		Hosts: []string{"http://192.168.31.102:2379"},
		Key:   "/server/rpc/shop",
	})); nil != err {
		t.Fatal(err)
	}
	time.AfterFunc(time.Second*6, func() {
		_ = server.Stop()
	})
	if err := server.Start(nil); nil != err {
		t.Fatal(err)
	}
}
func TestServer(t *testing.T) {
	server, err := SetUpServer(RpcServerConf{
		Name:     "rpc",
		ListenOn: ":8081",
		Etcd: discover.EtcdConf{
			Hosts: []string{"http://192.168.31.100:2379", "http://192.168.31.101:2379"},
			Key:   "/rpc/server/shop",
		},
		Health: true,
		Middlewares: ServerMiddlewaresConf{
			Recover: true,
		},
	})
	if nil != err {
		t.Fatal(err)
	}
	defer ShutDown(server)
	if err = Start(server, nil); nil != err {
		t.Fatal(err)
	}
	//time.AfterFunc(time.Second*6, func() {
	//	ShutDown(server)
	//})

}
